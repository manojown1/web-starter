import React from 'react';
import Home from '../pages/home'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom'

export default () => {
  return(
    <Router>
      <div className="App">

          <Route path="/" component={Home} />
    
      </div>
    </Router>
  )
}
