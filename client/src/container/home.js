 import React from 'react';
 import Header from '../component/header'

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick() {
    console.log(this); // React Component instance
  }
  render() {
    return (
      <div>
        <Header/>
      </div>
    );
  }
}
