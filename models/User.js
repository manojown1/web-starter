//Require Mongoose
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var UserSchema = new Schema({
  id: {
    type: String,
    required: true,
    unique: true
  },
  username: {
    type: String,
  },
  email: {
    type: String,
  },
  password: {
    type: Object,
    required: true
  },
}, {
  timestamps: true
});

mongoose.model('User', UserSchema);
