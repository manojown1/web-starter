//Require Mongoose
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var MovieSchema = new Schema({
  id: {
    type: String,
    unique: true
  },
  description: {
    type: String,
  },
  name: {
    type: String,
  },
  seatInfo: {
    type: Object,
    required: true
  },
}, {
  timestamps: true
});

mongoose.model('Movie', MovieSchema);
