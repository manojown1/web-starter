var mongoose = require('mongoose');
const Movie = mongoose.model('Movie')


// create a each screen
exports.createMovie = async (req, res) => {

  try {
    var movie = await Movie.create(req.body);
    res.send(200,{message:"movie setup successfully",movie:movie})
  }catch(e){
    console.log("error is ",e);
    res.send(403,{message:"Not able to setup movie"})
  }

}



// get All movie details
exports.getAllMovies = async (req, res) => {

  try {
    let movies = await Movie.find({});
    res.send(200,{message:"check your movie list",movies:movies})

  }catch(e){
    console.log("error is ",err);
  }

}


// get movie by perameter like name=inox
exports.getMovie = async (req, res) => {
  let name = req.params.name?req.params.name:null
  if(!name) return res.send(403,{message:"screen name is not provides. ex - screens/inox/"})

  try {
    let movies = await Movie.find({name:name});
    res.status(200).send({message:`Check your ${name} screen`,movies:movies})

  }catch(e){
    console.log("error is ",err);
  }

}


// To check availablity and get a reserve seat and filter seat by choice and seatnum
exports.getSeats = async (req, res) => {

  let name = req.params.name?req.params.name:null
  let status = req.query?req.query.status:null;
  let numSeats = req.query?req.query.numSeats:null;
  let choice = req.query?req.query.choice:null;



  try {

    let movie = await Movie.findOne({name:name});
    let seats = checkMovieAvailability(status,movie.seatInfo,numSeats,choice)

    if(!seats) return res.status(403).send({"message":`we don't have ${choice.slice(1,choice.length)} contigous seats for the user`})

    if(!choice)
      return res.send(200,{message:`Check your ${name} screen`,seats:seats.seats})
    else
      return res.send(200,{message:`Check your ${name} screen`,availableSeats:seats.available})


  }catch(e){

    console.log("error is ",e);

  }

}

// set or update reserve seat number
exports.setReserveSeat = async (req, res) => {

  let name = req.params.name?req.params.name:null;
  let reserveSeats = req.body.seats;

  if(!name) return res.send(403,{message:"screen name is not provides. ex - screens/inox/"});

  try {

    let movie = await Movie.findOne({name:name})

    if(!movie) return res.send({"message":"sorry no screen found with this name"}).status(403)

    for(i in reserveSeats){

        if(!movie.seatInfo[i]){
          return res.send(403,{"message":`Sorry ${i} row is not exist in this screen`})
        }

        movie.seatInfo[i].reserveSeats = reserveSeats[i];

    }

    let dataIs = await Movie.update({name:movie.name},movie)

    return res.status(200).send({message:`Check your ${name} screen`,movie:movie})

  }catch(e){

    console.log("error is ",e);
    res.status(500).send({message:`sorry something wend wrong`})

  }

}




// utility function for filtering and set seats as per status
function checkMovieAvailability(status,data,numSeats,choice){

  let seats = {}

  if(status === "unreserved"){

    for(i in data){
      seats[i] = reserveSeat(i,data);
    }

    return {seats}
  }

  else if(numSeats && choice){
      let reserveSeatcheck = reserveSeat(choice[0],data,choice);
      let available = [];
      let seatFound = reserveSeatcheck[parseInt(choice.slice(1,choice.length))];
      let totalSeatItration = parseInt(seatFound)+parseInt(numSeats);

      // check continuesly from higest seat value like choice A4 than 4 to 5,6,7...

      for(let i=seatFound;i<totalSeatItration;i++)
      {
          if(reserveSeatcheck[i]){

            available.push(reserveSeatcheck[i])

          }else{

            available = []
            totalSeatItration = seatFound-numSeats;
            // check continuesly from lowest seat value like choice A4 3,2,1...
            for(let i=totalSeatItration;i<=seatFound;i++)
            {

                if(reserveSeatcheck[i]){

                  available.push(reserveSeatcheck[i])

                }else{

                  return null;

                }

            }

          }
      }


      seats[choice[0]] = available;

      // seats[choice[0]] = reserveSeatcheck;

    return {available:seats};
  }

  return null;

}


// filter out reserve seat
function reserveSeat(i,data,choice){

  let arr = new Array(data[i].numberOfSeats)
  arr.fill(1)
  // filter seats as per aisleSeats
  if(choice && data[i].aisleSeats) data[i].aisleSeats.forEach((d) => {arr[d] = 0});
  if(data[i].reserveSeats) data[i].reserveSeats.forEach((d) => {arr[d] = 0});
  arr = arr.map((data,i) => {if(data) return i;});
  let ResrveSeat = [];
  if(choice) return arr;
  arr.forEach(data=>{if(data) ResrveSeat.push(data)})

  return ResrveSeat;
}
