var express = require('express');
var router = express.Router();
let Movie = require("./../controllers/movie")


/* GET users listing. */
router.post('/', Movie.createMovie);
router.get('/', Movie.getAllMovies);
router.get('/:name', Movie.getMovie);
router.post('/:name/reserve', Movie.setReserveSeat);
router.get('/:name/seats', Movie.getSeats);



module.exports = router;
