var express = require('express');
var router = express.Router();
let TedOps = require("../controllers")
/* GET home page. */
router.get('/', TedOps.getTags);

module.exports = router;
